package ruu.god.bankaccount;

/**
 * Класс, демонстрирующий пополнение счета и снятие с него денег
 *
 * @author Горбачева, 16ИТ18к
 */
public class Main {
    public static void main(String[] args) {
        BankAccount account = new BankAccount(1000);
        PutCash putCash = new PutCash(account);
        TakeCash takeCash = new TakeCash(account);
        putCash.start();
        takeCash.start();
    }
}
