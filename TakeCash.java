package ruu.god.bankaccount;

/**
 * Класс, предназначенный для создания потока, осуществляющего снятие денег со счета
 *
 * @author Горбачева, 16ИТ18к
 */
public class TakeCash extends Thread {
    /**
     * Текущий баланс
     */
    private final BankAccount account;

    TakeCash(BankAccount account) {
        this.account = account;
    }

    /**
     * Метод, позвляющий "снять" со счета заданную сумму
     * <p>
     * В случае, если на счете недостаточно среств для снятия нужной суммы, поток переходит
     * в режим ожидания. Как только достаточное количество средств появилось, вызывается метод,
     * уменьшающий значение баланса на заданную сумму, и выводится информация о текущем состоянии баланса
     */
    public void run() {
        for (int i = 0; i < 50; i++) {
            synchronized (account) {
                while (account.getBalance() < 2000) {
                    try {
                        System.out.println("Нужно подождать пополнения счета");
                        account.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                account.takeCash(2000);
                System.out.println("Мы только что сняли 2000 со счета, текущий баланс " + account.getBalance());
            }
        }
    }
}
