package ruu.god.bankaccount;

/**
 * Класс, предназначенный для создания потока, осуществляющего пополнение баланса
 *
 * @author Горбачева, 16ИТ18к
 */
public class PutCash extends Thread {
    /**
     * Текущий баланс
     */
    private final BankAccount account;

    PutCash(BankAccount account) {
        this.account = account;
    }

    /**
     * Метод, осуществляющий пополнение баланса
     * <p>
     * В методе происходит вызов метода, позволяющего увеличить значение текущего баланса
     * на заданную единицу. Также выводится информация о том, что баланс пополнен и его состояние на данный момент
     * После этого происходит пробуждение потоков, которые ожидали завершения даннного метода
     */
    public void run() {
        Thread.currentThread().setPriority(1);
        for (int i = 0; i < 100; i++) {
            synchronized (account) {
                account.putCash(1000);
                System.out.println("Мы только что пополнили баланс на 1000, текущий баланс " + account.getBalance());
                if (account.getBalance() >= 2000) {
                    account.notifyAll();
                }
            }
        }
    }
}
