package ruu.god.bankaccount;

/**
 * Класс-шаблон банковского счета
 *
 * @author Горбачева, 16ИТ18к
 */
class BankAccount {
    /**
     * Текущий баланс
     */
    private int balance;

    BankAccount(int balance) {
        this.balance = balance;
    }

    int getBalance() {
        return balance;
    }

    /**
     * Пополнение счета на заданную сумму
     *
     * @param money - сумма денег
     */
    void putCash(int money) {
        balance += money;
    }

    /**
     * Снятие со счета определенной суммы
     *
     * @param money - сумма денег
     */
    void takeCash(int money) {
        balance -= money;
    }
}
